package com.tryout.hemanth.expensetracker.service;

import com.tryout.hemanth.expensetracker.data.model.User;
import com.tryout.hemanth.expensetracker.data.repo.UserRepo;
import com.tryout.hemanth.expensetracker.exception.LoginException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(UserRepo.class)
public class LoginServiceTest {

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(UserRepo.class);
    }

    @Test(expected = LoginException.class)
    public void validateShouldThrowExceptionIfEmailIsNull() throws Exception {
        LoginService.validate(null, "password");
    }

    @Test(expected = LoginException.class)
    public void validateShouldThrowExceptionIfEmailIsEmpty() throws Exception {
        LoginService.validate("", "password");
    }

    @Test(expected = LoginException.class)
    public void validateShouldThrowExceptionIfEmailIsNotAnEmail() throws Exception {
        LoginService.validate("abcdef", "password");
    }

    @Test(expected = LoginException.class)
    public void validateShouldThrowExceptionIfPasswordIsNull() throws Exception {
        LoginService.validate("admin@test.com", null);
    }

    @Test(expected = LoginException.class)
    public void validateShouldThrowExceptionIfPasswordIsEmpty() throws Exception {
        LoginService.validate("admin@test.com", "");
    }

    @Test(expected = LoginException.class)
    public void validateShouldThrowExceptionIfPasswordIsLessThanFourCharacters() throws Exception {
        LoginService.validate("admin@test.com", "abc");
    }

    @Test
    public void shouldNotThrowExceptionIfBothEmailAndPasswordIsCorrect() throws Exception {
        try {
            LoginService.validate("admin@test.com", "abcd2345");
        } catch (Exception e) {
            assertTrue("Exception Thrown", false);
        }
    }

    @Test(expected = LoginException.class)
    public void shouldThrowExceptionIfUserDoesNotExists() throws Exception {
        String email = "email@gmail.com";
        String password = "password";
        when(UserRepo.getUserByEmail(email)).thenReturn(null);

        LoginService.login(email, password);
    }

    @Test(expected = LoginException.class)
    public void shouldThrowExceptionIfPasswordDoesntMatch() throws Exception {
        String email = "email@gmail.com";
        String password = "password";
        User value = new User();
        value.setEmail(email);
        value.setPassword(password);
        when(UserRepo.getUserByEmail(email)).thenReturn(value);

        LoginService.login(email, password + "1");
    }

    @Test
    public void shouldNotThrowExceptionIfLoginIsSuccessful() throws Exception {
        String email = "email@gmail.com";
        String password = "password";
        User value = new User();
        value.setEmail(email);
        value.setPassword(password);
        when(UserRepo.getUserByEmail(email)).thenReturn(value);

        try {
            LoginService.login(email, password);
        } catch (Exception e) {
            assertTrue("Exception Thrown", false);
        }
    }
}