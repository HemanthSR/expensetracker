package com.tryout.hemanth.expensetracker.service;

import com.tryout.hemanth.expensetracker.data.model.User;
import com.tryout.hemanth.expensetracker.data.repo.UserRepo;
import com.tryout.hemanth.expensetracker.exception.SignupException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(UserRepo.class)
public class SignupServiceTest {

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(UserRepo.class);
    }

    @Test(expected = SignupException.class)
    public void shouldThrowExceptionIfNameIsNull() throws Exception {
        SignupService.validate(null, "abc@gmail.com", "password");
    }

    @Test(expected = SignupException.class)
    public void shouldThrowExceptionIfNameIsEmpty() throws Exception {
        SignupService.validate("", "abc@gmail.com", "password");
    }

    @Test(expected = SignupException.class)
    public void shouldThrowExceptionIfNameIsLessThan3Characters() throws Exception {
        SignupService.validate("ab", "abc@gmail.com", "password");
    }

    @Test(expected = SignupException.class)
    public void shouldThrowExceptionIfEmailIsNull() throws Exception {
        SignupService.validate("name", null, "password");
    }

    @Test(expected = SignupException.class)
    public void shouldThrowExceptionIfEmailIsEmpty() throws Exception {
        SignupService.validate("name", "", "password");
    }

    @Test(expected = SignupException.class)
    public void shouldThrowExceptionIfEmailIsInvalid() throws Exception {
        SignupService.validate("abcd", "blablahblah", "password");
    }

    @Test(expected = SignupException.class)
    public void shouldThrowExceptionIfPasswordIsNull() throws Exception {
        SignupService.validate("name", "abc@gmail.com", null);
    }

    @Test(expected = SignupException.class)
    public void shouldThrowExceptionIfPasswordIsEmpty() throws Exception {
        SignupService.validate("name", "abc@gmail.com", "");
    }

    @Test(expected = SignupException.class)
    public void shouldThrowExceptionIfPasswordIsLessThan4Characters() throws Exception {
        SignupService.validate("abcd", "abc@gmail.com", "pas");
    }

    @Test
    public void shouldNotThrowExceptionIfAllTheInformationIsCorrect() throws Exception {
        try {
            SignupService.validate("name", "abc@gmail.com", "password");
        } catch (Exception e) {
            assertTrue("Should not throw exception", false);
        }
    }

    @Test(expected = SignupException.class)
    public void shouldNotSignupIfTheUserIsAlreadySignedUp() throws Exception {
        String password = "password";
        String email = "abc@gmail.com";
        String username = "name";
        when(UserRepo.getUserByEmail(email)).thenReturn(new User(username, email, password));

        SignupService.signup(username, email, password);
    }

    @Test
    public void shouldNotThrowExceptionIfNewUserSignedUp() throws Exception {
        String password = "password";
        String email = "abc@gmail.com";
        String username = "name";
        when(UserRepo.getUserByEmail(username)).thenReturn(null);

        try {
            SignupService.signup(username, email, password);

        } catch (Exception e) {
            assertTrue("Should signup", false);
        }
    }
}