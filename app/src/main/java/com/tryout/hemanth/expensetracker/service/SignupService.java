package com.tryout.hemanth.expensetracker.service;

import com.tryout.hemanth.expensetracker.data.model.User;
import com.tryout.hemanth.expensetracker.data.repo.UserRepo;
import com.tryout.hemanth.expensetracker.exception.SignupException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignupService {
    public static void validate(String name, String email, String password) {
        if (name == null || name.isEmpty() || name.length() < 3) {
            throw new SignupException("at least 3 characters");
        }
        if (email == null || email.isEmpty() || !checkEmailCorrect(email)) {
            throw new SignupException("enter a valid email address");
        }
        if (password == null || password.isEmpty() || password.length() < 4 || password.length() > 10) {
            throw new SignupException("between 4 and 10 alphanumeric characters");
        }
    }

    private static boolean checkEmailCorrect(String email) {
        String pttn = "^\\D.+@.+\\.[a-z]+";
        Pattern p = Pattern.compile(pttn);
        Matcher m = p.matcher(email);

        return m.matches();
    }

    public static void signup(String username, String email, String password) {
        User userByEmail = UserRepo.getUserByEmail(email);
        if (userByEmail != null) {
            throw new SignupException("You are already signed up.");
        }
        UserRepo.createUser(new User(username, email, password));
    }
}
