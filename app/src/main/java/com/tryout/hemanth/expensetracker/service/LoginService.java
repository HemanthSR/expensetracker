package com.tryout.hemanth.expensetracker.service;

import com.tryout.hemanth.expensetracker.data.model.User;
import com.tryout.hemanth.expensetracker.data.repo.UserRepo;
import com.tryout.hemanth.expensetracker.exception.LoginException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginService {
    public static void validate(String email, String password) {
        if (email == null || email.isEmpty() || !checkEmailCorrect(email)) {
            throw new LoginException("Enter valid email address");
        }
        if (password == null || password.isEmpty() || password.length() < 4) {
            throw new LoginException("Password should be between greater than 4 alphanumeric characters");
        }
    }

    public static void login(String email, String password) {
        User userByEmail = UserRepo.getUserByEmail(email);
        if (userByEmail == null) {
            throw new LoginException("User not exists.");
        }
        if (!userByEmail.getPassword().equals(password)) {
            throw new LoginException("Password didn't match.");
        }
    }

    private static boolean checkEmailCorrect(String email) {
        String pttn = "^\\D.+@.+\\.[a-z]+";
        Pattern p = Pattern.compile(pttn);
        Matcher m = p.matcher(email);

        return m.matches();
    }
}
