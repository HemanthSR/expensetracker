package com.tryout.hemanth.expensetracker.exception;

public class SignupException extends RuntimeException {
    public SignupException(String detailMessage) {
        super(detailMessage);
    }
}
