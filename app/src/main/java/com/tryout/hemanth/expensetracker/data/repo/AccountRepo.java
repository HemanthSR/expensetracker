package com.tryout.hemanth.expensetracker.data.repo;

import com.tryout.hemanth.expensetracker.data.model.Account;
import com.tryout.hemanth.expensetracker.data.model.Currency;
import com.tryout.hemanth.expensetracker.data.model.User;

public class AccountRepo {
    public static String createTable() {
        return "CREATE TABLE " + Account.TABLE_NAME + "("
                + Account.COLUMN_ACCOUNT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Account.COLUMN_NAME + " TEXT,"
                + Account.COLUMN_DESCRIPTION + " TEXT,"
                + Account.COLUMN_INITIAL_BALANCE + " REAL,"
                + Account.COLUMN_CURRENCY + " INTEGER,"
                + Account.COLUMN_USER + " INTEGER,"
                + " FOREIGN KEY(" + Account.COLUMN_CURRENCY + ") REFERENCES " + Currency.TABLE_NAME + "(" + Currency.COLUMN_CURRENCY_ID + ")"
                + " FOREIGN KEY(" + Account.COLUMN_USER + ") REFERENCES " + User.TABLE_NAME + "(" + User.COLUMN_USER_ID + "))";
    }

    public static String dropTable() {
        return "DROP TABLE IF EXISTS " + Account.TABLE_NAME;
    }

}
