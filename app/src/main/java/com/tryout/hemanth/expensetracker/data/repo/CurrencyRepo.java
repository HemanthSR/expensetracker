package com.tryout.hemanth.expensetracker.data.repo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.tryout.hemanth.expensetracker.data.DatabaseManager;
import com.tryout.hemanth.expensetracker.data.model.Currency;

import java.util.ArrayList;
import java.util.List;

public class CurrencyRepo {
    public static String createTable() {
        return "CREATE TABLE " + Currency.TABLE_NAME + "("
                + Currency.COLUMN_CURRENCY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Currency.COLUMN_SHORT_NAME + " TEXT,"
                + Currency.COLUMN_FULL_NAME + " TEXT)";
    }

    public static String dropTable() {
        return "DROP TABLE IF EXISTS " + Currency.TABLE_NAME;
    }

    public static String initialData() {
        return "INSERT INTO " + Currency.TABLE_NAME + "(" + Currency.COLUMN_SHORT_NAME + "," + Currency.COLUMN_FULL_NAME + ") values (\"INR\", \"Indian Rupees\")";
    }

    public static List<Currency> getAll() {
        try {

            SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();

            Cursor cursor = db.rawQuery("SELECT * from " + Currency.TABLE_NAME, null);

            List<Currency> currencies = new ArrayList<>();
            if (cursor.moveToFirst()) {
                Currency currency = new Currency();
                currency.setCurrencyId(Integer.parseInt(cursor.getString(0)));
                currency.setShortName(cursor.getString(1));
                currency.setFullName(cursor.getString(2));
                currencies.add(currency);
            }
            return currencies;
        } finally {
            DatabaseManager.getInstance().closeDatabase();
        }
    }
}
