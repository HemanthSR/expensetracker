package com.tryout.hemanth.expensetracker.data.repo;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.tryout.hemanth.expensetracker.data.DatabaseManager;
import com.tryout.hemanth.expensetracker.data.model.User;

public class UserRepo {
    public static String createTable() {
        return "CREATE TABLE " + User.TABLE_NAME + "("
                + User.COLUMN_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + User.COLUMN_USERNAME + " TEXT,"
                + User.COLUMN_PASSWORD + " TEXT,"
                + User.COLUMN_EMAIL + " TEXT )";
    }

    public static String dropTable() {
        return "DROP TABLE IF EXISTS " + User.TABLE_NAME;
    }

    public static int createUser(User user) {
        try {
            int userId;
            SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();

            ContentValues values = new ContentValues();
            values.put(User.COLUMN_USERNAME, user.getUsername());
            values.put(User.COLUMN_EMAIL, user.getEmail());
            values.put(User.COLUMN_PASSWORD, user.getPassword());

            userId = (int) db.insert(User.TABLE_NAME, null, values);

            return userId;
        } finally {
            DatabaseManager.getInstance().closeDatabase();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    public static User getUserByEmail(String email) {
        try {

            SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();

            Cursor cursor = db.rawQuery("SELECT * from " + User.TABLE_NAME + " u where u.email=\'" + email + "\'", null);

            if (cursor.moveToFirst()) {
                User user = new User();
                user.setUserId(Integer.parseInt(cursor.getString(0)));
                user.setUsername(cursor.getString(1));
                user.setPassword(cursor.getString(2));
                user.setEmail(cursor.getString(3));
                return user;
            }
        } finally {
            DatabaseManager.getInstance().closeDatabase();
        }
        return null;
    }
}
