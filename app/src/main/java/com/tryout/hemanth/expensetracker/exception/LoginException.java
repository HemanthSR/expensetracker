package com.tryout.hemanth.expensetracker.exception;

public class LoginException extends RuntimeException {
    public LoginException(String message) {
        super(message);
    }
}
