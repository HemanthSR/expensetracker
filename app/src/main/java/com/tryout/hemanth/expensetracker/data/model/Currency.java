package com.tryout.hemanth.expensetracker.data.model;

public final class Currency {
    public static final String TABLE_NAME = "currency";

    public static final String COLUMN_CURRENCY_ID = "currency_id";
    public static final String COLUMN_SHORT_NAME = "short_name";
    public static final String COLUMN_FULL_NAME = "full_name";

    private Integer currencyId;
    private String shortName;
    private String fullName;

    public Currency(String shortName, String fullName, String initialBalance) {
        this.shortName = shortName;
        this.fullName = fullName;
    }

    public Currency(Integer userId, String shortName, String fullName, String initialBalance) {
        this.currencyId = userId;
        this.shortName = shortName;
        this.fullName = fullName;
    }

    public Currency() {
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
