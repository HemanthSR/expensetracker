package com.tryout.hemanth.expensetracker.data;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.tryout.hemanth.expensetracker.app.App;
import com.tryout.hemanth.expensetracker.data.repo.AccountRepo;
import com.tryout.hemanth.expensetracker.data.repo.CurrencyRepo;
import com.tryout.hemanth.expensetracker.data.repo.UserRepo;

public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "empensetracker.db";
    private static final String TAG = DBHelper.class.getSimpleName();

    public DBHelper() {
        super(App.getContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //All necessary tables you like to create will create here
        db.execSQL(UserRepo.createTable());
        db.execSQL(CurrencyRepo.createTable());
        db.execSQL(AccountRepo.createTable());

        db.execSQL(CurrencyRepo.initialData());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, String.format("SQLiteDatabase.onUpgrade(%d -> %d)", oldVersion, newVersion));

        // Drop table if existed, all data will be gone!!!
        db.execSQL(AccountRepo.dropTable());
        db.execSQL(UserRepo.dropTable());
        db.execSQL(CurrencyRepo.dropTable());
        onCreate(db);
    }
}
