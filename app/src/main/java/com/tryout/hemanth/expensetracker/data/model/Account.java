package com.tryout.hemanth.expensetracker.data.model;

public final class Account {
    public static final String TABLE_NAME = "account";

    public static final String COLUMN_ACCOUNT_ID = "account_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_INITIAL_BALANCE = "initial_balance";
    public static final String COLUMN_CURRENCY = "currency";
    public static final String COLUMN_USER = "user";

    private Integer accountId;
    private String name;
    private String description;
    private Double initialBalance;
    private Currency currency;
    private User user;

    public Account(String name, String description, Double initialBalance, Currency currency, User user) {
        this.name = name;
        this.description = description;
        this.initialBalance = initialBalance;
        this.currency = currency;
        this.user = user;
    }

    public Account(Integer userId, String name, String description, Double initialBalance, Currency currency, User user) {
        this.accountId = userId;
        this.name = name;
        this.description = description;
        this.initialBalance = initialBalance;
        this.currency = currency;
        this.user = user;
    }

    public Account() {
    }
}
